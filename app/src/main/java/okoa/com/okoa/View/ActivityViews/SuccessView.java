package okoa.com.okoa.View.ActivityViews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import okoa.com.okoa.R;

public class SuccessView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_view);
    }
}
