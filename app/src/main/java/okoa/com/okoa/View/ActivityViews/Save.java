package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.AwesomeTextView;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapDropDown;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;

public class Save extends AppCompatActivity {

    private android.widget.ImageButton backbutton;
    private android.widget.ImageButton notification;
    private android.widget.LinearLayout header;
    private com.beardedhen.androidbootstrap.AwesomeTextView savedFee;
    private com.beardedhen.androidbootstrap.BootstrapDropDown paymenttype;
    private com.beardedhen.androidbootstrap.BootstrapDropDown paymentdetails;
    private String[] mobile_money_data = {"MTN Mobile Money", "Airtel Money"};
    private String[] bank_data = {"Pride Microfinance", "Stanbic Bank", "DFCU Bank", "Barclays Bank"};
    private com.beardedhen.androidbootstrap.BootstrapButton proceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        this.proceed = (BootstrapButton) findViewById(R.id.proceed);
        this.paymentdetails = (BootstrapDropDown) findViewById(R.id.payment_details);
        this.paymenttype = (BootstrapDropDown) findViewById(R.id.payment_type);
        this.savedFee = (AwesomeTextView) findViewById(R.id.savedFee);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.notification = (ImageButton) findViewById(R.id.notification);
        this.backbutton = (ImageButton) findViewById(R.id.back_button);

        Utility utility = new Utility(this);
        String savedFeeString = utility.getPref(Constants.savedFee);
        if(savedFeeString.isEmpty()){
            savedFee.setText("UGX 50,000");
        }else{
            savedFee.setText("UGX "+savedFeeString);
        }
        paymenttype.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                Log.e("Id selected ", id + "");
                if (id == 0) {
                    paymenttype.setText("MOBILE MONEY");
                    paymentdetails.setText("Choose mobile money network");
                    paymentdetails.setDropdownData(mobile_money_data);
                } else {
                    paymenttype.setText("BANK");
                    paymentdetails.setText("Choose your bank");
                    paymentdetails.setDropdownData(bank_data);
                }
            }
        });
        paymentdetails.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                if(paymenttype.getText().toString().equalsIgnoreCase("mobile money")){
                    paymentdetails.setText(mobile_money_data[id]);
                }else if(paymenttype.getText().toString().equalsIgnoreCase("bank")){
                    paymentdetails.setText(bank_data[id]);
                }else{
                    paymentdetails.setText("");
                }
            }
        });
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
    }
}
