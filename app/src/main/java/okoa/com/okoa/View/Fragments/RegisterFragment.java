package okoa.com.okoa.View.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {


    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout topbar;
    private com.beardedhen.androidbootstrap.BootstrapEditText fullname;
    private com.beardedhen.androidbootstrap.BootstrapEditText email;
    private com.beardedhen.androidbootstrap.BootstrapEditText phonenumber;
    private com.beardedhen.androidbootstrap.BootstrapEditText nextofkin;
    private com.beardedhen.androidbootstrap.BootstrapButton continuebutton;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private BootstrapEditText nextofkinphone;
    Utility utility;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_register, container, false);
        this.nextofkinphone = (BootstrapEditText) v.findViewById(R.id.next_of_kin_phone);
        this.continuebutton = (BootstrapButton) v.findViewById(R.id.continue_button);
        this.nextofkin = (BootstrapEditText) v.findViewById(R.id.next_of_kin);
        this.phonenumber = (BootstrapEditText) v.findViewById(R.id.phone_number);
        this.email = (BootstrapEditText) v.findViewById(R.id.email);
        this.fullname = (BootstrapEditText) v.findViewById(R.id.full_name);
        this.topbar = (LinearLayout) v.findViewById(R.id.top_bar);
        this.backbutton = (ImageButton) v.findViewById(R.id.back_button);

        utility = new Utility(getActivity());

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        continuebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFieldContent();
            }
        });
        return v;
    }

    private void saveFieldContent(){
        if(Utility.validateTextField(nextofkinphone) &&
                Utility.validateTextField(nextofkin) &&
                Utility.validateTextField(phonenumber) &&
                Utility.validateTextField(email) &&
                Utility.validateTextField(fullname)){

            utility.savePref(Constants.nextOfKinPhone, nextofkinphone.getText().toString());
            utility.savePref(Constants.nextOfKinName, nextofkin.getText().toString());
            utility.savePref(Constants.phonenumber, phonenumber.getText().toString());
            utility.savePref(Constants.email, email.getText().toString());
            utility.savePref(Constants.fullname, fullname.getText().toString());

            ApproveSaccoFrag approveSaccoFrag = new ApproveSaccoFrag();
            fragmentTransaction.replace(R.id.activity_login__join_sacco, approveSaccoFrag, "approveSaccoFrag");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }else{
            Utility.showToast(getActivity(),"Please all fields are required!");
        }

    }
}
