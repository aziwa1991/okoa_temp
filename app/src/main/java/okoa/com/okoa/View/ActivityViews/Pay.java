package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapDropDown;

import okoa.com.okoa.R;

public class Pay extends AppCompatActivity {

    private android.widget.ImageButton notification;
    private android.widget.LinearLayout header;
    private com.beardedhen.androidbootstrap.BootstrapDropDown mobiletopup;
    private com.beardedhen.androidbootstrap.BootstrapDropDown paybill;
    private ImageButton backbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        this.backbutton = (ImageButton) findViewById(R.id.back_button);
        this.paybill = (BootstrapDropDown) findViewById(R.id.pay_bill);
        this.mobiletopup = (BootstrapDropDown) findViewById(R.id.mobile_top_up);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.notification = (ImageButton) findViewById(R.id.notification);

        mobiletopup.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                //show dialog
            }
        });

        paybill.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                //show bills dialog
            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
    }
}
