package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;

import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;
import okoa.com.okoa.View.Fragments.LoginFrag;
import okoa.com.okoa.View.Fragments.SelectOptions;

public class Login_JoinSacco extends AppCompatActivity {

    private android.widget.RelativeLayout activityloginjoinsacco;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    public Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__join_sacco);
        this.activityloginjoinsacco = (RelativeLayout) findViewById(R.id.activity_login__join_sacco);

        utility = new Utility(Login_JoinSacco.this);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        SelectOptions selectOptions = new SelectOptions();
        fragmentTransaction.add(R.id.activity_login__join_sacco, selectOptions, "selectOptions");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
