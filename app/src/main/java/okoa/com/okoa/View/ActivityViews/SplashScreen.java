package okoa.com.okoa.View.ActivityViews;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;
import okoa.com.okoa.View.ActivityViews.IntroViews.IntroView;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*Delay for 3seconds and then display login screen
        * */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Utility utility = new Utility(SplashScreen.this);
                if(utility.getPref(Constants.shownIntro).isEmpty()){
                    Utility.startActivity(SplashScreen.this, IntroView.class);
                }else {
                    if (utility.getPref(Constants.isLoggedIn).isEmpty()) {
                        Utility.startActivity(SplashScreen.this, Login_JoinSacco.class);
                        finish();
                    } else {
                        Utility.startActivity(SplashScreen.this, Home.class);
                        finish();
                    }
                }
            }
        },3000);
    }
}
