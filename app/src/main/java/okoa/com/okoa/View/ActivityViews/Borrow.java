package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapDropDown;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.beardedhen.androidbootstrap.BootstrapWell;

import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;

public class Borrow extends AppCompatActivity {

    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout header;
    private com.beardedhen.androidbootstrap.BootstrapWell well;
    private com.beardedhen.androidbootstrap.BootstrapEditText amount;
    private com.beardedhen.androidbootstrap.BootstrapDropDown payouttype;
    private com.beardedhen.androidbootstrap.BootstrapEditText accountdetails;
    private android.widget.RelativeLayout activityborrow;
    private com.beardedhen.androidbootstrap.BootstrapButton processloan;
    private BootstrapDropDown paymentperiod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrow);
        this.paymentperiod = (BootstrapDropDown) findViewById(R.id.payment_period);
        this.processloan = (BootstrapButton) findViewById(R.id.process_loan);
        this.activityborrow = (RelativeLayout) findViewById(R.id.activity_borrow);
        this.accountdetails = (BootstrapEditText) findViewById(R.id.account_details);
        this.payouttype = (BootstrapDropDown) findViewById(R.id.payout_type);
        this.amount = (BootstrapEditText) findViewById(R.id.amount);
        this.well = (BootstrapWell) findViewById(R.id.well);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.backbutton = (ImageButton) findViewById(R.id.back_button);

        paymentperiod.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                switch (id){
                    case 0:
                        paymentperiod.setText("1 month");
                        break;
                    case 1:
                        paymentperiod.setText("4 months");
                        break;
                    case 2:
                        paymentperiod.setText("8 months");
                        break;
                    case 3:
                        paymentperiod.setText("12 months");
                        break;
                    default: break;
                }
            }
        });

        payouttype.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                switch (id){
                    case 0:
                        accountdetails.setHint("Enter mobile money number");
                        payouttype.setText("Mobile Money");
                        break;
                    case 1:
                        accountdetails.setHint("Enter bank account number");
                        payouttype.setText("Bank");
                        break;
                    default:
                        break;
                }
            }
        });
        processloan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.startActivity(Borrow.this,SuccessView.class);
            }
        });
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
    }
}
