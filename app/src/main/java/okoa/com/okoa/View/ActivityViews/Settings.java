package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import okoa.com.okoa.Controller.Configs.RecyclerItemClickListener;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.Controller.SettingsAdapter;
import okoa.com.okoa.R;

public class Settings extends AppCompatActivity {

    private android.widget.ImageButton backbutton;
    private android.widget.ImageButton notification;
    private android.widget.LinearLayout header;
    private android.support.v7.widget.RecyclerView recyclerView;
    private android.widget.LinearLayout activitysettings;
    RecyclerView.Adapter settingsAdapter;
    RecyclerView.LayoutManager layoutManager;

    String[] settingsContent = {"Profile","Transaction Summary","FAQs","Contacts","Terms and Conditions","Tips"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.activitysettings = (LinearLayout) findViewById(R.id.activity_settings);
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.notification = (ImageButton) findViewById(R.id.notification);
        this.backbutton = (ImageButton) findViewById(R.id.back_button);

        layoutManager = new LinearLayoutManager(this);
        settingsAdapter = new SettingsAdapter(settingsContent,Settings.this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(settingsAdapter);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(Settings.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position){
                    case 0:
                        //go to profile
                        Utility.startActivity(Settings.this,Profile.class);
                        break;
                    case 1:
                        //transactions
                        break;
                    case 2:
                        //FAQs
                        break;
                    case 3:
                        //contacts
                        break;
                    case 4:
                        //terms
                        break;
                    default:
                        break;
                }
            }
        }));
    }
}
