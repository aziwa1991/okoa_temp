package okoa.com.okoa.View.ActivityViews.IntroViews;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;
import okoa.com.okoa.View.ActivityViews.Login_JoinSacco;

/**
 * Created by aliziwa on 5/28/17.
 */

public class IntroView extends AppIntro {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        addSlide(AppIntroFragment.newInstance("",
                "Earn interest from all transactions",
                R.drawable.trend,
                getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance("",
                "Manage and monitor your accounts as you set goals",
                R.drawable.group,
                getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance("",
                "Get tips on saving and sound investment advice",
                R.drawable.tips,
                getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance("",
                "Are you in a fix, assistance is a touch away",
                R.drawable.cross,
                getResources().getColor(R.color.colorPrimary)));

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);
        Utility utility = new Utility(IntroView.this);
        utility.savePref(Constants.shownIntro,"yes");

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Utility.startActivity(IntroView.this, Login_JoinSacco.class);
        finish();
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Utility.startActivity((IntroView.this), Login_JoinSacco.class);
        finish();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}