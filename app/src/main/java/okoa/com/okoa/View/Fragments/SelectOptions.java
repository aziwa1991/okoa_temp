package okoa.com.okoa.View.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beardedhen.androidbootstrap.BootstrapButton;

import okoa.com.okoa.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectOptions extends Fragment {

    private com.beardedhen.androidbootstrap.BootstrapButton login;
    private com.beardedhen.androidbootstrap.BootstrapButton joinsacco;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    public SelectOptions() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_select_options, container, false);
        this.joinsacco = (BootstrapButton) v.findViewById(R.id.join_sacco);
        this.login = (BootstrapButton) v.findViewById(R.id.login);

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginFrag loginFrag = new LoginFrag();
                fragmentTransaction.replace(R.id.activity_login__join_sacco, loginFrag, "loginFrag");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        joinsacco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                fragmentTransaction.replace(R.id.activity_login__join_sacco, registerFragment, "registerFragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return v;
    }

}
