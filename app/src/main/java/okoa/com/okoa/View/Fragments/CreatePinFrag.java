package okoa.com.okoa.View.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import okoa.com.okoa.Controller.ApplicationCore;
import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.Model.AccountModel;
import okoa.com.okoa.Model.OkoaCoreModel;
import okoa.com.okoa.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePinFrag extends Fragment {


    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout topbar;
    private com.beardedhen.androidbootstrap.BootstrapEditText pin;
    private com.beardedhen.androidbootstrap.BootstrapEditText repin;
    private com.beardedhen.androidbootstrap.BootstrapButton submit;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ApplicationCore core;
    Utility utility;

    public CreatePinFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_pin, container, false);
        this.submit = (BootstrapButton) v.findViewById(R.id.submit);
        this.repin = (BootstrapEditText) v.findViewById(R.id.re_pin);
        this.pin = (BootstrapEditText) v.findViewById(R.id.pin);
        this.topbar = (LinearLayout) v.findViewById(R.id.top_bar);
        this.backbutton = (ImageButton) v.findViewById(R.id.back_button);

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        utility = new Utility(getActivity());

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.validateTextField(pin) && Utility.validateTextField(repin)){
                    if(pin.getText().toString().equals(repin.getText().toString())){
                        postRegistrationData();
                    }
                }
            }
        });
        return v;
    }

    private void postRegistrationData(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Posting registration data...");
        progressDialog.show();

        core = new ApplicationCore();
        AccountModel accountModel = new AccountModel(utility.getPref(Constants.fullname),
                utility.getPref(Constants.phonenumber),utility.getPref(Constants.email),pin.getText().toString(),
                utility.getPref(Constants.saccoId), utility.getPref(Constants.nextOfKinName),utility.getPref(Constants.nextOfKinPhone));
        core.networkCall().joinSacco(accountModel)
                .enqueue(new Callback<OkoaCoreModel>() {
                    @Override
                    public void onResponse(Call<OkoaCoreModel> call, Response<OkoaCoreModel> response) {
                        progressDialog.dismiss();
                        if(response.body().getAccepted()){
                            taketoLogin();
                        }else {
                            Utility.showToast(getActivity(),response.body().getDescription());
                        }
                    }

                    @Override
                    public void onFailure(Call<OkoaCoreModel> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
    }

    private void taketoLogin(){
        LoginFrag loginFrag = new LoginFrag();
        fragmentTransaction.replace(R.id.activity_login__join_sacco, loginFrag, "loginFrag");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
