package okoa.com.okoa.View.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.firebase.iid.FirebaseInstanceId;

import okoa.com.okoa.Controller.ApplicationCore;
import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.Controller.NetworkCalls.NetworkCalls;
import okoa.com.okoa.Model.LoginModel;
import okoa.com.okoa.Model.OkoaCoreModel;
import okoa.com.okoa.R;
import okoa.com.okoa.View.ActivityViews.Home;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFrag extends Fragment {


    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout topbar;
    private com.beardedhen.androidbootstrap.BootstrapEditText phonenumber;
    private com.beardedhen.androidbootstrap.BootstrapEditText pin;
    private com.beardedhen.androidbootstrap.BootstrapButton loginbutton;
    NetworkCalls networkCalls;

    public LoginFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login, container, false);
        networkCalls = new NetworkCalls();
        this.loginbutton = (BootstrapButton) v.findViewById(R.id.login_button);
        this.pin = (BootstrapEditText) v.findViewById(R.id.pin);
        this.phonenumber = (BootstrapEditText) v.findViewById(R.id.phone_number);
        this.topbar = (LinearLayout) v.findViewById(R.id.top_bar);
        this.backbutton = (ImageButton) v.findViewById(R.id.back_button);
        // Inflate the layout for this fragment

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Utility.startActivity(getActivity(), Home.class);
                getActivity().finish();*/
               if(Utility.validateTextField(phonenumber) && Utility.validateTextField(pin)){
                   loginUser(phonenumber.getText().toString(), pin.getText().toString());
               }else{
                   Utility.showToast(getActivity(),"Please all fields are required");
               }
            }
        });
        return v;
    }


    /**
     *
     * @param username
     * @param password
     */
    private void loginUser(String username, String password){

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Logging in...");
        progressDialog.show();

        LoginModel loginModel = new LoginModel(username,password);
        networkCalls.loginUser(loginModel,progressDialog,getActivity());

    }

}
