package okoa.com.okoa.View.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import okoa.com.okoa.Controller.ApplicationCore;
import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.Model.OkoaCoreModel;
import okoa.com.okoa.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApproveSaccoFrag extends Fragment {


    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout topbar;
    private com.beardedhen.androidbootstrap.BootstrapEditText pin;
    private com.beardedhen.androidbootstrap.BootstrapButton confirmsacco;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ApplicationCore core;
    Utility utility;

    public ApproveSaccoFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_approve_sacco, container, false);
        this.confirmsacco = (BootstrapButton) v.findViewById(R.id.confirm_sacco);
        this.pin = (BootstrapEditText) v.findViewById(R.id.pin);
        this.topbar = (LinearLayout) v.findViewById(R.id.top_bar);
        this.backbutton = (ImageButton) v.findViewById(R.id.back_button);

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        utility = new Utility(getActivity());

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        confirmsacco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.validateTextField(pin)){
                    verifySacco(pin.getText().toString());
                }else{
                    Utility.showToast(getActivity(),"Enter sacco code");
                }
            }
        });

        return v;
    }

    /**
     * verifies sacco
     * @param saccoId
     */
    private void verifySacco(String saccoId){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Verifying sacco code");
        progressDialog.show();

        core = new ApplicationCore();
        core.networkCall().checkSaccoValidity(saccoId).enqueue(new Callback<OkoaCoreModel>() {
            @Override
            public void onResponse(Call<OkoaCoreModel> call, Response<OkoaCoreModel> response) {
                progressDialog.dismiss();
                if(response.body().getAccepted()){
                    goToCreatePin();
                    utility.savePref(Constants.saccoId,pin.getText().toString());
                }else{
                    Utility.showToast(getActivity(),response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<OkoaCoreModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void goToCreatePin(){
        CreatePinFrag createPinFrag = new CreatePinFrag();
        fragmentTransaction.replace(R.id.activity_login__join_sacco, createPinFrag, "createPinFrag");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}
