package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;

public class Profile extends AppCompatActivity {

    private android.widget.ImageButton backbutton;
    private android.widget.LinearLayout header;
    private com.beardedhen.androidbootstrap.BootstrapEditText setgoal;
    private com.beardedhen.androidbootstrap.BootstrapButton updategoal;
    private android.widget.LinearLayout activityprofile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        this.activityprofile = (LinearLayout) findViewById(R.id.activity_profile);
        this.updategoal = (BootstrapButton) findViewById(R.id.update_goal);
        this.setgoal = (BootstrapEditText) findViewById(R.id.set_goal);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.backbutton = (ImageButton) findViewById(R.id.back_button);

        final Utility utility = new Utility(this);
        String savedFee = utility.getPref(Constants.savedFee);
        if(savedFee.isEmpty()){
            setgoal.setText("50,000");
        }else{
            setgoal.setText(savedFee);
        }

        updategoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.validateTextField(setgoal)){
                    try{
                        int new_goal = Integer.parseInt(setgoal.getText().toString());
                        utility.savePref(Constants.savedFee, String.valueOf(new_goal));
                        setgoal.setEnabled(false);
                        Utility.showToast(Profile.this,"Goal updated");
                    }catch (Exception e){
                        Utility.showToast(Profile.this,"Enter a valid number");
                    }
                }
            }
        });
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
    }
}
