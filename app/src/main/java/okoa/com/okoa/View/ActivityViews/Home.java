package okoa.com.okoa.View.ActivityViews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.beardedhen.androidbootstrap.AwesomeTextView;

import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.R;

public class Home extends AppCompatActivity {

    private android.widget.ImageButton notification;
    private android.widget.LinearLayout header;
    private com.beardedhen.androidbootstrap.AwesomeTextView saccocontribution;
    private android.widget.RelativeLayout activityhome;
    private ImageButton save;
    private ImageButton borrow;
    private ImageButton pay;
    private ImageButton settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.settings = (ImageButton) findViewById(R.id.settings);
        this.pay = (ImageButton) findViewById(R.id.pay);
        this.borrow = (ImageButton) findViewById(R.id.borrow);
        this.save = (ImageButton) findViewById(R.id.save);
        this.activityhome = (RelativeLayout) findViewById(R.id.activity_home);
        this.saccocontribution = (AwesomeTextView) findViewById(R.id.sacco_contribution);
        this.header = (LinearLayout) findViewById(R.id.header);
        this.notification = (ImageButton) findViewById(R.id.notification);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.startActivity(Home.this, Pay.class);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.startActivity(Home.this,Save.class);
            }
        });
        borrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.startActivity(Home.this,Borrow.class);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.startActivity(Home.this,Settings.class);
            }
        });
    }
}
