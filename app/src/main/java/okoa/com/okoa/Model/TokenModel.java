package okoa.com.okoa.Model;

/**
 * Created by aliziwa on 5/28/17.
 */
public class TokenModel {

    private String token;
    private String phone;

    public TokenModel(String token, String phone) {
        this.token = token;
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
