package okoa.com.okoa.Model;

/**
 * Created by aliziwa on 5/27/17.
 */

public class AccountModel {
    private String fullName;
    private String phone;
    private String email;
    private String password;
    private String groupId;
    private String kinName;
    private String kinPhone;

    public AccountModel(String fullName, String phone, String email, String password, String groupId, String kinName, String kinPhone) {
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.groupId = groupId;
        this.kinName = kinName;
        this.kinPhone = kinPhone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getKinName() {
        return kinName;
    }

    public void setKinName(String kinName) {
        this.kinName = kinName;
    }

    public String getKinPhone() {
        return kinPhone;
    }

    public void setKinPhone(String kinPhone) {
        this.kinPhone = kinPhone;
    }
}
