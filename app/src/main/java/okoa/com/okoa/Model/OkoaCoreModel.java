package okoa.com.okoa.Model;

/**
 * Created by aliziwa on 5/26/17.
 */

public class OkoaCoreModel {
    private Boolean accepted;
    private String description;
    private String data;


    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
