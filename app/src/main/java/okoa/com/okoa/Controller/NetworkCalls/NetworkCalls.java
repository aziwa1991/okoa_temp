package okoa.com.okoa.Controller.NetworkCalls;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import okoa.com.okoa.Controller.ApplicationCore;
import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Controller.Configs.Utility;
import okoa.com.okoa.Model.LoginModel;
import okoa.com.okoa.Model.OkoaCoreModel;
import okoa.com.okoa.Model.TokenModel;
import okoa.com.okoa.View.ActivityViews.Home;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aliziwa on 5/28/17.
 */

public class NetworkCalls {
    ApplicationCore core = new ApplicationCore();

    /**
     * This function sends the latest token to the server
     * @param tokenModel
     */
    public void sendTokenToServer(TokenModel tokenModel){

        core.networkCall().updateToken(tokenModel).enqueue(new Callback<OkoaCoreModel>() {
            @Override
            public void onResponse(Call<OkoaCoreModel> call, Response<OkoaCoreModel> response) {
                if(response.body().getAccepted()){
                    System.out.println("Updated token ...");
                }else{
                    System.out.println("Error ..."+response.body().getDescription());
                }
            }

            @Override
            public void onFailure(Call<OkoaCoreModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void loginUser(final LoginModel loginModel, final ProgressDialog progressDialog, final Context context){
        core.networkCall().login(loginModel).enqueue(new Callback<OkoaCoreModel>() {
            @Override
            public void onResponse(Call<OkoaCoreModel> call, Response<OkoaCoreModel> response) {
                progressDialog.dismiss();
                try{
                    if(response.body().getAccepted()){
                        Utility utility = new Utility(context);
                        utility.savePref(Constants.isLoggedIn, "yes");
                        utility.savePref(Constants.phonenumber,loginModel.getUsername());

                        //send FCM token
                        String token = FirebaseInstanceId.getInstance().getToken();
                        Log.e("Token ninja ",token+" phone number "+loginModel.getUsername());
                        sendTokenToServer(new TokenModel(token,utility.getPref(Constants.phonenumber)));
                        Utility.startActivity(context, Home.class);
                        Activity activity = (Activity) context;
                        activity.finish();
                    }else{
                        Utility.showToast(context,response.body().getDescription());
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    Utility.showToast(context,"User doesn't exist");
                }

            }

            @Override
            public void onFailure(Call<OkoaCoreModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

}
