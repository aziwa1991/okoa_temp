package okoa.com.okoa.Controller.Configs;

/**
 * Created by aliziwa on 5/27/17.
 */

public class Constants {
    //public static final String baseURL = "http://192.168.1.203:8080/";
    public static final String baseURL = "http://212.47.246.249:8080/";
    public static String nextOfKinPhone = "nextOfKinPhone";
    public static String nextOfKinName = "nextOfKinName";
    public static String phonenumber = "phoneNumber";
    public static String email = "email";
    public static String fullname = "fullname";
    public static String saccoId = "saccoId";
    public static String savedFee = "savedFee";
    public static String isLoggedIn = "isLoggedIn";
    public static String shownIntro = "showIntro";
}
