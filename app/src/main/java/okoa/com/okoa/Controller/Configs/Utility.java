package okoa.com.okoa.Controller.Configs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.EditText;
import android.widget.Toast;

import okoa.com.okoa.R;

/**
 * Created by aliziwa on 5/26/17.
 */

public class Utility {

    private SharedPreferences mSharedPreferences;
    private Context mContext;

    /**
     *
     * @param context
     */
    public Utility(Context context){
        String PREFERENCE_NAME = "okoa";
        mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }
    /**
     * Starts new activity
     * @param context
     * @param newActivity
     */
    public static void startActivity(Context context, Class<?> newActivity){
        Activity activity = (Activity) context;
        Intent startNewActivity = new Intent(context, newActivity);
        activity.startActivity(startNewActivity);
        activity.overridePendingTransition(R.anim.activity_in,R.anim.activity_out);
    }

    /**
     *
     * @param key
     * @param value
     */
    public void savePref(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }
    /**
     *
     * @param key
     * @param defValue
     * @return
     */
    public String getPref(String key, String... defValue) {
        if (defValue.length > 0)
            return mSharedPreferences.getString(key, defValue[0]);
        else
            return mSharedPreferences.getString(key, "");
    }

    /**
     *
     * @param editText
     * @return
     */
    public static Boolean validateTextField(EditText editText){
        if(editText.getText().toString().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    /**
     *
     * @param context
     * @param s
     */
    public static void showToast(Context context, String s) {
        Toast.makeText(context,s,Toast.LENGTH_LONG).show();
    }
}
