package okoa.com.okoa.Controller.NetworkCalls;

import okoa.com.okoa.Controller.Configs.Constants;
import okoa.com.okoa.Model.AccountModel;
import okoa.com.okoa.Model.LoginModel;
import okoa.com.okoa.Model.OkoaCoreModel;
import okoa.com.okoa.Model.TokenModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by aliziwa on 5/26/17.
 */

public interface NetworkCallInterface {


    @Headers("Content-Type: application/json")
    @POST(Constants.baseURL+"setup/join")
    Call<OkoaCoreModel> joinSacco(@Body AccountModel accountModel);

    @Headers("Content-Type: application/json")
    @POST(Constants.baseURL+"security/login")
    Call<OkoaCoreModel> login(@Body LoginModel loginModel);

    @Headers("Content-Type: application/json")
    @GET(Constants.baseURL+"setup/check/{groupId}")
    Call<OkoaCoreModel> checkSaccoValidity(@Path("groupId") String groupId);

    @Headers("Content-Type: application/json")
    @POST(Constants.baseURL+"profile/token")
    Call<OkoaCoreModel> updateToken(@Body TokenModel tokenModel);
}
